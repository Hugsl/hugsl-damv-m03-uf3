using System;

namespace ParametritzacioMetodes
{
    public class CAMPSITEORGANIZER
    {
        public void Start()
        {
            string nom;
            int num = 0;
            int control=0;
            string[,] llista = new string[1000,1000];
            do
            {
                nom = Console.ReadLine()?.ToUpper();
                if (nom!="END")
                {
                 
                    switch (nom)
                    {   case "ENTRA":
                        llista = Insert(nom,num,llista,control);
                        control++;
                        break;
                        case "MARXA": 
                            
                            if (control>0)
                            {   llista = Delete(nom,num,llista);
                                control--;
                            }
                            else
                            {
                                Console.Write(" NO hi ha ningú que pugui marxar   ");
                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("veritat, Hugo...");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                            
                            break;
                    }   
                }

            } while (nom!="END");
        }

        private string[,] Insert(string nom, int num,  string[,] llista,int control)
        {
            bool same;
            do
            {nom = Console.ReadLine()?.ToUpper(); 
             
                same = Array.IndexOf(llista, nom)>=0;
                if (same)
                {
                    Console.WriteLine("Aquest nom ja existeix, inserta un altre");
                }
            } while (same);
            num = Convert.ToInt32(Console.ReadLine());
            
            llista[control, 0] = nom;
            llista[0, control] = Convert.ToString(num);

            for (int i = 0; i < llista.GetLength(0); i++)
            { ;
                for (int j = 0; j < llista.GetLength(1); j++)
                {
                    Console.Write(llista[i, j]);
                }
            }
            return llista;
        }

        private string[,] Delete(string nom, int num, string[,] llista)
        {
            int control;
            
            nom = Console.ReadLine()?.ToUpper(); 
            control = Array.IndexOf(llista, nom);
            if (control>=0)
            {
                string[,] newllista = new string[llista.GetLength(0)-1 ,llista.GetLength(1)-1];
                
                return newllista;
                
            }

            return llista;
        }
    }
}