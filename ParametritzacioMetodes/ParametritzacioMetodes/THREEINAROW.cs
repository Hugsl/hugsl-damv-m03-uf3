using System;

namespace ParametritzacioMetodes
{
    public class Threeinarow
    {
        public void Start()
        {
            Console.Clear();
            Console.WriteLine("You're in THREEINAROW");

            int winner;
            winner = Game();
            if (winner == 1)
            {
                Console.WriteLine("Player 'X' Wins");
            }
            else if (winner == 2)
            {
                Console.WriteLine("Player 'O' Wins");
            }
            else if (winner == 3)
            {
                Console.WriteLine("Tie");
            }
            else {
                for (int i = 0; i < 10000; i++)
                {
                    Console.WriteLine(" ERROR ");
                }

            }

            Console.ReadKey();


        }

        private int Game()
        {
            int winner = 0;
            char[,] taulell = new char[,]
            {
                {
                    '_', '_', '_'
                },
                {
                    '_', '_', '_'
                },
                {
                    '_', '_', '_'
                }
            };


            //0 == P1 | 1 == P2 
            int turn = 0;
            // char a == line | char b == column
            int a;
            int b;
            int count=0;
            do
            {
                if ( count>8)
                {
                    winner = 3;
                    return winner;
                }
                do
                {

                   
                    a = Seta();
                    b = Setb();
                    Console.Clear();
                    if (turn != 2)
                    {
                        if (taulell[a, b] == 'X' ||
                            taulell[a, b] == 'O')
                        {
                            Console.WriteLine("LLOC OCUPAT");
                        }
                        else if (turn == 0)
                        {
                            taulell[a, b] = 'X';
                            turn++;
                        }
                        else if (turn == 1)
                        {
                            taulell[a, b] = 'O';
                            turn--;
                        }
                       
                    }

                    
                } while (taulell[Convert.ToInt32(a), Convert.ToInt32(b)] == '_');

                Taulell(taulell);

                winner = CheckWinner(taulell, turn);
                
               
                
                count++;
            } while (winner == 0);

            return winner;
        }

        private void Taulell(char[,] taulell)
        {
            Console.WriteLine($"0 {taulell[0, 0]} | {taulell[0, 1]} | {taulell[0, 2]}");
            Console.WriteLine($"1 {taulell[1, 0]} | {taulell[1, 1]} | {taulell[1, 2]}");
            Console.WriteLine($"2 {taulell[2, 0]} | {taulell[2, 1]} | {taulell[2, 2]}");
        }

        private int Seta()
        {
            int a;
            do
            {
                a = Convert.ToInt32(Console.ReadLine());
            } while (a != 0 && a != 1 && a != 2);

            return a;
        }

        private int Setb()
        {
            int b;
            do
            {
                b = Convert.ToInt32(Console.ReadLine());
            } while (b != 0 && b != 1 && b != 2);

            return b;
        }

        private int CheckWinner(char[,] taulell, int turn)
        {
            int winner;
            if (turn == 1)
            {
                winner = CheckWinnerX(taulell);
            }
            else
            {
                winner = CheckWinnerO(taulell);
               
            }

            return winner;
        }


        public static int CheckWinnerX(char[,] taulell)
        {
            int winner = 0;
            int winnerX = 0;

            for (int j = 0; j < 3 && winnerX < 3; j++)
            {
                for (int i = 0; i < taulell.GetLength(0) || winnerX == 3; i++)
                {
                    if (taulell[i, j] == 'X')
                    {
                        winnerX++;
                    }
                }

                if (winnerX < 3)
                {
                    winnerX = 0;
                    for (int i = 0; i < taulell.GetLength(1); i++)
                    {
                        if (taulell[j, i] == 'X')
                        {
                            winnerX++;
                        }
                    }

                    if (winnerX < 3)
                    {
                        winnerX = 0;
                        if ((taulell[0, 0] == 'X' && taulell[1, 1] == 'X' && taulell[2, 2] == 'X') ||
                            (taulell[0, 2] == 'X' && taulell[1, 1] == 'X' && taulell[2, 0] == 'X'))
                        {
                            winnerX = 3;
                        }

                    }

                }

            }


            if (winnerX == 3)
            {
                winner = 1;
            }

            return winner;
        }
        private static int CheckWinnerO(char[,] taulell)
        {
            int winner = 0;
            int winnerO = 0;

            for (int j = 0; j < 3 && winnerO<3; j++)
            {
                for (int i = 0; i < taulell.GetLength(0) || winnerO == 3; i++)
                {
                    if (taulell[i, j] == 'O')
                    {
                        winnerO++;
                    }
                }

                if (winnerO <3)
                {
                    winnerO = 0;
                    for (int i = 0; i < taulell.GetLength(1); i++)
                    {
                        if (taulell[j, i] == 'O')
                        {
                            winnerO++;
                        }
                    }

                    if (winnerO < 3)
                    { winnerO = 0;
                        if ((taulell[0, 0] == 'O' && taulell[1, 1] == 'O' && taulell[2, 2] == 'O') ||
                            (taulell[0, 2] == 'O' && taulell[1, 1] == 'O' && taulell[2, 0] == 'O'))
                        {
                            winnerO = 3;
                        }
                      
                    }
                  
                }

            }
            if (winnerO == 3)
            {
                winner = 1;
            }
            return winner;
        }

    }
}