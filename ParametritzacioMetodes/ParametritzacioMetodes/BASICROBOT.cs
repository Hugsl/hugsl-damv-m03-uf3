using System;
using System.ComponentModel.Design;

namespace ParametritzacioMetodes
{
    public class BASICROBOT
    {
        public void Start()
        {
            double[] numbers = new double[]{0.00,0.00,1.00};
            string code;

            do
            {
                code=Console.ReadLine()?.ToUpper();
                if (code !="END")
                {
                  numbers = Commands(numbers,code);
                }
                
            } while (code!="END");
            
        }

        private double[] Commands(double[] numbers, string code)
        {switch (code)
            {
                case "DALT":
                    numbers[1] += numbers[2];
                    break;
                    
                case "BAIX":
                    numbers[1] -= numbers[2];
                    break;
                    
                case "DRETA":
                    numbers[0] += numbers[2];
                    break;
                    
                case "ESQUERRA":
                    numbers[0] -= numbers[2];
                    break;
                    
                case "ACCELERAR":
                    if (numbers[2]<10)
                    {
                        numbers[2] += 0.50;
                    }
                    else
                    {
                        Console.WriteLine("La velocitat no pot ser més gran que 10");
                    }
                    break;
                    
                case "DISMINUIR":
                    if (numbers[2]>0)
                    {
                        numbers[2] -= 0.50;
                    }
                    else
                    {
                        Console.WriteLine("La velocitat no pot ser menor que 0");
                    }
                    break;
                    
                case "POSICIO":
                    Console.WriteLine($"La posició del robot és ({numbers[0]}  ,   {numbers[1]})");
                    break;
                    
                case "VELOCITAT":
                    Console.WriteLine($"La velocitat actual del robot és de: {numbers[2]}");
                    break;
            }
            return numbers;
        }
    }
}
