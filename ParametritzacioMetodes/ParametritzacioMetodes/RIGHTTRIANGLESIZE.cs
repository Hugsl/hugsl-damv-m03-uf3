using System;

namespace ParametritzacioMetodes
{
    public class RIGHTTRIANGLESIZE
    {
        public void Start() {
        int tringnum = Convert.ToInt32(Console.ReadLine()); 
        double[] TrianglLenght= new double[tringnum];
        double[] TrianBase = new double[tringnum];
        double[] hipot =new double[tringnum] ;
        for (int i = 0; i < tringnum; i++)
        {
            TrianglLenght[i] = Convert.ToDouble(Console.ReadLine());
            TrianBase[i]= Convert.ToDouble(Console.ReadLine());
        }
        for (int i = 0; i < tringnum; i++)
        {
            hipot[i] = Math.Sqrt(Math.Pow(TrianglLenght[i],2 )+Math.Pow(TrianBase[i],2)); 
            Console.WriteLine($"La superfície del triangle és:{Superficie(hipot[i], TrianBase[i])} i el seu perímetre és de {Perimetre(TrianglLenght[i],TrianBase[i],hipot[i])}");
            
        }

        Console.ReadKey();
        }

        public static double Superficie(double a, double b)
        {
            var c = (a * b) / 2;
            return c;
        }

        private double Perimetre(double a, double b, double c)
        {
            double d = a + b + c;
            return d;
        }
    }
}