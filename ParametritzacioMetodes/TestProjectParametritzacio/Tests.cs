﻿using System;
using NUnit.Framework;
using ParametritzacioMetodes;

namespace TestProjectParametritzacio
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Light()
        {
            Assert.AreEqual(true,LAMP.Light("TURN ON",false));
        }

        [Test]
        public void Superficie()
        {
            Assert.AreEqual(6,RIGHTTRIANGLESIZE.Superficie(6,2));
        }

        [Test]
        public void CheckWinnerX()
        {
            Assert.AreEqual(1,Threeinarow.CheckWinnerX(  new char[,]
            {
                {'X', 'X', 'X'}, {'_', '_', '_'}, {'_', '_', '_'}}));
        }

        [Test]
        public void Count()
        {
            Assert.AreEqual(new int[,]{{0,1},{0,0}}, Squashcounter.Count(new int[,] {{9, 0}, {0, 0}},'a','a'));
        }
        }
        
    }