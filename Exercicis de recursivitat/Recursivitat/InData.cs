using System;
namespace Recursivitat
{
    public class InData
    {
        public void Ex1()
        {
            
            int n = Convert.ToInt32(Console.ReadLine());
            int coso=Recursivity.Ex1R(n-1);
            Console.WriteLine(coso);
            Console.ReadKey();
        }

        public void Ex2()
        {
            
            int n;
            int m;
            int coso=0;
            do
            {n = Convert.ToInt32(Console.ReadLine()); 
                m = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
            } while (n<m && n<1 && m <=1);
            
            coso = Recursivity.Ex2R(n,m,coso);
            Console.WriteLine(coso);
            Console.ReadKey();
        }
        public void Ex3()
        {
            Recursivity a = new Recursivity();
            int n = Convert.ToInt32(Console.ReadLine());
            int m = Convert.ToInt32(Console.ReadLine());
            int coso=0;
              coso  = Recursivity.Ex3R(n,m);
            Console.WriteLine(coso);
            Console.ReadKey();
        }

        public void Ex4()
        {   
            int n = Convert.ToInt32(Console.ReadLine());
            int coso=0;
            int[] a =new int[n];
            Random b = new Random();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = b.Next(-30, 30);
                Console.Write($"{a[i]} , ");
            }
            Console.WriteLine();
            coso = Recursivity.Ex4R(a, n-1, coso);
            Console.WriteLine(coso);
            Console.ReadKey();
        }
    }
}