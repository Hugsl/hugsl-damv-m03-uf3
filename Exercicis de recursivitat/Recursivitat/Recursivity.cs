namespace Recursivitat
{
    public class Recursivity
    {
        public static int Ex1R(int n)
        {
            if (n<=0)
            {
                return 0;
            }

            if (n == 1)
            {
                return 1;
            }

            return 3 * Ex1R(n - 1) + 2 * Ex1R(n - 2);
        }

        public static int Ex2R(int n, int m, int coso)
        {
            if (m < n)
            {
                n = n - m;
                coso++;
                return Ex2R(n, m,coso);
            }
            else
            {
                return coso+=1;
            }
           
        }
        public static int Ex3R(int n, int m)
        {
            if (n==m || n==1 || m==0 )
            {
                return 1;
            }
            else
            {
                return Ex3R(n - 1, m) + Ex3R(n - 1, m - 1);
            }
        }

        public static int Ex4R(int[] a, int n, int coso)
        {
            if (n == -1)
            {
                return coso;
            }
            else
            {
                coso = a[n] + coso;
                return Ex4R(a, n - 1, coso);
            }
            
        }
    }
}
