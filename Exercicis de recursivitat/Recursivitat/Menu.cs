using System;
namespace Recursivitat
{
    public class Menu
    {
        public void Start()
        {
            char a;
            do
            {
                InData b = new InData();
               a=Console.ReadKey().KeyChar;
                switch (a)
                {
                    case '1':
                        Console.Clear();
                        b.Ex1();
                        break;
                    case '2':
                        Console.Clear();
                       b.Ex2();
                        break;
                    case '3':
                        Console.Clear();
                        b.Ex3();
                        break;
                    case '4':
                        Console.Clear();
                        b.Ex4();
                        break;
                }
                Console.Clear();
            } while (a!='e');
        }

        
    }
}