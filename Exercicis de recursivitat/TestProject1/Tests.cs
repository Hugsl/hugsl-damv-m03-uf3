﻿using System;
using NUnit.Framework;
using Recursivitat;

namespace TestProject1
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Ex1()
        {
            Assert.AreEqual(11,Recursivity.Ex1R(3));
            Assert.AreEqual(5,Recursivity.Ex2R(10,2,0));
            Assert.AreEqual(1,Recursivity.Ex3R(1,0));
            Assert.AreEqual(-20, Recursivity.Ex4R(new int[]{10,-30},1,0));
        }
        
    }
}