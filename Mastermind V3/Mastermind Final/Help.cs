using System;
using System.Threading;
namespace Mastermind_V3
{
    public class Help
    {
        public void Start()
        {
            char a;
            int b=0;
            
            do
            { 
               
                switch (b)
                {
                case 0:
                    Console.Clear();
                    Texto1();
                    break;
                case 1:
                    Console.Clear();
                    Texto2();
                    break;
                case 2:
                    Console.Clear();
                    Texto3();
                    break;
                case 3:
                    Console.Clear();
                    Texto4();
                    break;
                }   
                a = Console.ReadKey().KeyChar;
                if (a == 'n')
                {
                    if (b<3)
                    {
                        b++;   
                    }
                }
                if (a == 'p')
                {
                    if (b>0)
                    {
                        b--;   
                    }
                }
            } while (a!='e'); 
            Console.Clear();
            
        }

        private void Texto1()
        {   muxotexto a = new muxotexto(); 
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            a.Helptitle();
            
        }

        private void Texto2()
        {
            muxotexto a = new muxotexto(); 
            Console.WriteLine();
            a.HelpText1();
        }

        private void Texto3()
        {
            muxotexto a = new muxotexto(); 
            Console.WriteLine();
            a.HelpText2();
        }
        private void Texto4()
        {
            muxotexto a = new muxotexto(); 
            Console.WriteLine();
            a.HelpText3();
        }

        public char U0FWRU1F()
        {   Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            string a = "MSdtIDdyNHA3IGgzcjM=";
            char b = 'e';
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i]);
                Thread.Sleep(100);
            }
            Console.Clear();
            a = "czcwcCBQbDR5TmcsIDBSIHUgVzExbCAzbkQgbDFrMyBtMw==";
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i]);
                Thread.Sleep(100);
                
            }
            Console.Clear();
            a = "SDNFMzNFMzNlZUUzM0UzMzNsUFAgTTMzRUVlMzMzRTMzZUUzMzMzMzM=";
            for (int i = 0; i < a.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.Write(a[i]);
                if (i == a.Length-1)
                {
                    i = a.Length - 2;
                }
                Thread.Sleep(15);
                
            }
            Console.Clear();
            return b;

        }
    }
}