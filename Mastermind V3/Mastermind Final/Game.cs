using System;
using System.Linq;

namespace Mastermind_V3
{
    public class Game
    {
        public int Start()
        {
            muxotexto a = new muxotexto();
            Console.Clear();
            int win = 0;
            char[] secret;
            if (Select()== true)
            {
                secret = Randomez();
            }
            else
            {
               secret = Randomno();
               
            }

            if (GameStart(secret))
            {   a.WinText();
                win+=1;
            }
            else
            {
                a.LooseText();
            }
            Console.ReadKey();
            Console.Clear();
            return win;
        }
        private bool Select()
        {
            bool b;
            char a;
            Console.WriteLine("Easy mode? y/N ");
            a = Console.ReadKey().KeyChar;
            if (a == 'y')
            {
                b = true;
            }
            else
            {
                b = false;
            }

            return b;
        }

        private char[] Randomez()
        {
            char[] secret = new char[4];
            Random random = new Random();
            for (int creation = 0; creation < 4; creation++)
            {
                int randomnum;
                char lletra;
                bool repeteix; //Amb aixo L'edu m'ha ajudat
                do
                {
                    randomnum = random.Next(0, 6);
                    lletra = (char) ('a' + randomnum);
                    secret[creation] = lletra;
                    repeteix = false;
                    for (int a = 0; a < creation; a++) repeteix = repeteix || secret[creation] == secret[a];
                } while (repeteix);
               
            }
            Console.WriteLine();
            return secret;
        }

        private char[] Randomno()
        {   char[] secret = new char[4];
            Random random = new Random();
            for (int creation = 0; creation < 4; creation++)
            {
                int randomnum;
                char lletra;
                randomnum = random.Next(0,6);
                lletra = (char) ('a' + randomnum);
                secret[creation] = lletra;
            }
            Console.WriteLine();
            return secret;
        }

        private bool GameStart(char[] secret)
        {   int[] GB= new int[2];
            bool win=false;
            char[] complet = new char [4];
            int intents = 0;
            

            do
            {
                Create(complet,secret);
               
                GB= CheckGB(complet, secret);
                
                if (GB[0] == 4)
                {  
                    win = true;
                }
                else
                {
                   
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write($" Tens {GB[0]} lletres ben colocades ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"I Tens {GB[1]} mal colocades!");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("");
                }
                
                intents++;
            } while (intents < 12 && win!=true);

            return win;
        }

        private char[] Create(char[] complet,char[] secret)
        {
            char input;
            for (int caracter = 0; caracter < 4; caracter++)
            {
                do
                {
                    //bucle per posibles errors de caracter
                    input = Convert.ToChar(Console.ReadKey().KeyChar);
                    if (input != 'a' && input != 'b' && input != 'c' && input != 'd' && input != 'e' &&
                        input != 'f')
                    {   Console.Clear();
                       
                        Console.WriteLine();
                        Console.WriteLine("ESCRIU LLETRA ENTRE A -> F");
                        for (int i = 0; i < caracter; i++)
                        {
                            Console.Write(complet[i]);
                        }
                    }
                    //Console.WriteLine("  "+imput+ "Dins del bucle");
                } while (input != 'a' && input != 'b' && input != 'c' && input != 'd' && input != 'e' &&
                         input != 'f');

                complet[caracter] = input;
                //Console.WriteLine("  " + complet[caracter]);
            }

            return complet;
        }
     
        
        private int[] CheckGB(char[] complet, char[] secret)
        {
            char[] allchar = new char[] {'a', 'b', 'c', 'd', 'e', 'f'};
            int[] GB = new int[2];
            int[] af =ChPass(secret);
 
            int[] pass = ChCode(complet);

            for (int i = 0; i < complet.Length; i++)
            {
                if (complet[i]==secret[i])
                {
                    GB[0]++;
                }
                else
                {
                    for (int check = 0; check < allchar.Length; check++)
                    {
                        if (af[check] == pass[check])
                        {
                            GB[1]++;
                        }
                    }
                }
            }
            GB[1] = GB[1] / 6;
            return GB;
        }

        private int[] ChPass( char[] secret)
        {
            int[] afcomapre = new int[]{'a','b','c','d','e','f'};
            int[] afresult = new int[6];
            for (int i = 0; i < secret.Length; i++)
            {
                for (int j = 0; j < afcomapre.Length; j++)
                {
                    if (afcomapre[j] == secret[i])
                    {
                        afresult[j]++;
                    }
                 
                    
                }
              
            }
            

            return afresult;
        }

        private int[] ChCode(char[] complet)
        {
            int[] afcomapre = new int[]{'a','b','c','d','e','f'};
            int[] afresult = new int[6];
            for (int i = 0; i < complet.Length; i++)
            {
                for (int j = 0; j < afcomapre.Length; j++)
                {
                    if (afcomapre[j] == complet[i])
                    {
                        afresult[j]++;
                    }
                 
                     
                }
            }
            return afresult;
        }
        }
    }
