using System;
using System.Threading;

namespace Mastermind_V3
{
    public class muxotexto
    {

        public void advertisment()
        {
            Console.WriteLine();
            
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(" EL JOC HA DE SER JUGAT EN UNA CONSOLA EXTERNA I LES MESURES D'AQUESTA HAN DE SER 100X25");
            Thread.Sleep(10000);
            Console.Clear();
        }
        
        public void Title(int a)
        {
            {
                Console.WriteLine($"Games winned = {a} ");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("     ███    ███  █████  ███████ ████████ ███████ ██████      ███    ███ ██ ███    ██ ██████   ");
                Console.WriteLine("     ████  ████ ██   ██ ██         ██    ██      ██   ██     ████  ████ ██ ████   ██ ██   ██  ");
                Console.WriteLine("     ██ ████ ██ ███████ ███████    ██    █████   ██████      ██ ████ ██ ██ ██ ██  ██ ██   ██  ");
                Console.WriteLine("     ██  ██  ██ ██   ██      ██    ██    ██      ██   ██     ██  ██  ██ ██ ██  ██ ██ ██   ██  ");
                Console.WriteLine("     ██      ██ ██   ██ ███████    ██    ███████ ██   ██     ██      ██ ██ ██   ████ ██████");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("                                        Press Space to Play                                         ");
                Console.WriteLine("                                          Press h to Help                                         ");
                Console.WriteLine("                                          Press e to Exit                                         ");
            }
        }

        public void WinText()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();Console.WriteLine();
            Console.WriteLine();Console.WriteLine();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                    ██    ██  ██████  ██    ██     ██     ██ ██ ███    ██  ██ ");
            Console.WriteLine("                     ██  ██  ██    ██ ██    ██     ██     ██ ██ ████   ██  ██ ");
            Console.WriteLine("                      ████   ██    ██ ██    ██     ██  █  ██ ██ ██ ██  ██  ██");
            Console.WriteLine("                       ██    ██    ██ ██    ██     ██ ███ ██ ██ ██  ██ ██    ");
            Console.WriteLine("                       ██     ██████   ██████       ███ ███  ██ ██   ████  ██");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("                                    Press Any Key To Continue");
        }

        public void LooseText()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(
                "      @@@ @@@   @@@@@@   @@@  @@@     @@@        @@@@@@    @@@@@@    @@@@@@   @@@@@@@@  ");
            Console.WriteLine(
                "      @@@ @@@  @@@@@@@@  @@@  @@@     @@@       @@@@@@@@  @@@@@@@@  @@@@@@@   @@@@@@@@  ");
            Console.WriteLine("      @@! !@@  @@!  @@@  @@!  @@@     @@!       @@!  @@@  @@!  @@@  !@@       @@!  ");
            Console.WriteLine("      !@! @!!  !@!  @!@  !@!  @!@     !@!       !@!  @!@  !@!  @!@  !@!       !@! ");
            Console.WriteLine(
                "       !@!@!   @!@  !@!  @!@  !@!     @!!       @!@  !@!  @!@  !@!  !!@@!!    @!!!:!    ");
            Console.WriteLine(
                "       @!!!   !@!  !!!  !@!  !!!     !!!       !@!  !!!  !@!  !!!   !!@!!!   !!!!!:    ");
            Console.WriteLine("        !!:    !!:  !!!  !!:  !!!     !!:       !!:  !!!  !!:  !!!       !:!  !!:  ");
            Console.WriteLine("        :!:    :!:  !:!  :!:  !:!      :!:      :!:  !:!  :!:  !:!      !:!   :!:  ");
            Console.WriteLine(
                "         ::    ::::: ::  ::::: ::      :: ::::  ::::: ::  ::::: ::  :::: ::    :: ::::  ");
            Console.WriteLine("          :      : :  :    : :  :      : :: : :   : :  :    : :  :   :: : :    : :: ::");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("                            Press Any Key To Continue........");
        }

        public void Helptitle()
        {
            Console.WriteLine("  ███    ███ ███████ ███    ██ ██    ██     ██████   ██   █████       ██ ██    ██ ██████   █████  ");
            Console.WriteLine("  ████  ████ ██      ████   ██ ██    ██     ██   ██  █   ██   ██      ██ ██    ██ ██   ██ ██   ██ ");
            Console.WriteLine("  ██ ████ ██ █████   ██ ██  ██ ██    ██     ██   ██      ███████      ██ ██    ██ ██   ██ ███████ ");
            Console.WriteLine("  ██  ██  ██ ██      ██  ██ ██ ██    ██     ██   ██      ██   ██ ██   ██ ██    ██ ██   ██ ██   ██ ");
            Console.WriteLine("  ██      ██ ███████ ██   ████  ██████      ██████       ██   ██  █████   ██████  ██████  ██   ██");
            Console.WriteLine();
            Console.WriteLine(); Console.WriteLine(); Console.WriteLine(); Console.WriteLine();
            HelpMenu();
        }

        public void HelpText1()
        {  Console.Write("\n Beningut al Mastermind! \n \n Aquest joc tracta de adivinar una contrasenya secreta de cuatre caraters (de la A a la F) generats \n aleatoriament, en el cual tens 12 intents per adivinar la contrasenya. \n" +
                        "\n Hi ha dos modes: \n \n " +
                        " - Fàcil: Els caràcters no es repeteixen \n\n" +
                        "  - Difícil: Les lletres es poden repetir \n" +
                        "\n\n Per defecte el mode es el difícil, però abans de cada partida et preguntara per corretgir la \n dificultat.\n" +
                        " \n\n Continua a la següent pàgina  ---->");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            HelpMenu();
        }

        public void HelpText2()
        {  Console.Write("\n Com jugar al Mastermind: \n \n Un cop entris a la partida, s'haura generat el codi i hauras d'introduir els caractrers un a un.\n" +
                         "\n Avisos:\n \n " +
                         " - NO podras corretgir els caracters que hagis introduit \n\n" +
                         "  - NO podras incerir caracters que no estiguin entre la A i la F. \n" +
                         "\n\n Un cop hagis inserit tots els caracters et corretgira la contrasenya. \n" +
                         " \n\n Continua a la següent pàgina  ---->");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            HelpMenu();
        }
        public void HelpText3()
        {
            Console.WriteLine(
                "\n Correcció de la Contrasenya: \n \n Després de incerir els caràcters, t'apareixera una frase en la cual:\n\n ");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(" - T'apareixera en color verd la quantitat de caracters ven posats. \n");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(" - I en vermell els que estiguin mal colocats. \n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n Cada cop que insereixis una contrasenya perdras una vida, si encertes tots els caracters guanyaras \n " +
                              "directament.\n" +
                                                                             " \n\n -_- <Fins aquí el tutorial) ");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            HelpMenu();
        }

        private void HelpMenu()
        {
            Console.WriteLine("                                 Press 'n' to see the next page");
            Console.WriteLine("                               Press 'p' to see the previous page");
            Console.WriteLine("                                       Press 'e' to exit");
        }
    }
}